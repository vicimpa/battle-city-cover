import Game from "./game";
import Vec2 from './vec2'
import Textures from './textures'

export interface DamageObject{
    new (...args): {
        damage(): void
    }
}

interface Owner{
    position: Vec2
    dirrection: number
    damages: DamageObject[]
    shells: Shell[]
}

export default class Shell{
    public dirrection: number = 0
    public position: Vec2
    public delay: number = 0.01
    public startDelay: number = this.delay
    public anim: number = 0

    constructor(public game: Game, public owner: Owner, public speed: number = 10) {
        this.dirrection = owner.dirrection
        this.position = owner.position.clone()
        this.owner.shells.push(this)
        this.game.add(this)
    }

    public get texture() {
        let {dirrection, anim} = this
        let t = Textures.get('shells').sector(16, 16, 40 / 2, 40 / 2)

        return (ctx: CanvasRenderingContext2D, x: number, y: number) => {
            t.draw(ctx, anim ? anim : dirrection, 0, x * 2 + 0.45, y * 2 +0.45)
        }
    }

    public drop() {
        this.owner.shells = []
    }

    public update() {
        let {dirrection} = this
        let {now} = this.game

        let s = now * this.speed

        switch(dirrection){
            case 0:
                    this.position.add(0, -s)
                break;
            case 1:
                    this.position.add(-s, 0)
                break;
            case 2:
                    this.position.add(0, s)
                break;
            case 3:
                    this.position.add(s, 0)
                break;
        }
        
        if(this.position.range(12.5, 12.5, -0.4, -0.4)){
            if(this.delay > 0){
                if(this.anim < 4)
                    this.anim = 4

                this.delay -= now
            }else if(this.anim < 7) {
                this.delay = this.startDelay
                this.anim ++
            }
            else this.game.del(this)
        }
    }

    public render() {
        let {position, game} = this
        let {ctx} = game.scene
        this.texture(ctx, position.x, position.y)
    }
}