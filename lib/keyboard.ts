export default class Keyboard{
    public keys: string[] = []

    constructor(public debug: boolean = false) {
        window.addEventListener('keydown', (e: KeyboardEvent) => {
            let index = this.keys.indexOf(e.key)

            if(index === -1)
                this.keys.push(e.key)

            if(debug)
                console.log(this.keys)
        })

        window.addEventListener('keyup', (e: KeyboardEvent) => {
            let index = this.keys.indexOf(e.key)

            if(index !== -1)
                this.keys.splice(index, 1)

            if(debug)
                console.log(this.keys)
        })

        window.addEventListener('blur', () => {
            while(this.keys.length)
                this.keys.pop()
                
            if(debug)
                console.log(this.keys)
        })
    }

    public isKey(key: string) {
        return this.keys.indexOf(key) !== -1
    }
}